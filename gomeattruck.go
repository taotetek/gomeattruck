package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/taotetek/pq"
	zmq "github.com/taotetek/zmq4"
	"io/ioutil"
	"log"
	"math"
	"time"
)

type MeatTruck struct {
	Receiver *zmq.Socket  // zmq socket that receives meatspace stats
	Reactor  *zmq.Reactor // zmq reactor loop
	Dbconn   *sql.DB      // postgres connection handle
	ConnInfo string       // postgres connection string
	Config   struct {
		Endpoint   string // zmq endpoint
		Restpoint  string // rest endpoint
		Dbuser     string // postgres user name
		Dbname     string // postgres db name
		Dbpassword string // postgresl db password
		Sslmode    string // use ssl mode or not
		Dbhost     string // postgres database host
	}
}

func (m *MeatTruck) LoadConf(path string) error {
	confFile, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}

	if err := json.Unmarshal(confFile, &m.Config); err != nil {
		log.Fatal(err)
	}

	m.ConnInfo = fmt.Sprintf(
		"user=%s dbname=%s password=%s sslmode=%s host=%s\n",
        m.Config.Dbuser, m.Config.Dbname, m.Config.Dbpassword,
		m.Config.Sslmode, m.Config.Dbhost)

	return nil
}

type StatMsg struct {
	Epoch_Ms    int64     // millis since epoch (from stat msg)
	Interval    int64     // current hour in seconds since epoch
	intTime     time.Time // current hour in time format
	Fingerprint string    // client fingerprint
	Topic       string    // zmq topic
}

func (s *StatMsg) LoadJSON(topic string, payload string) error {
	b := []byte(payload)
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	s.Topic = topic
	s.Interval = int64(math.Floor(float64(s.Epoch_Ms/1000)/3600) * 3600)
	s.intTime = time.Unix(s.Interval, 0)
	return nil
}

func (s *StatMsg) Persist(db *sql.DB) error {
	var exists int
	err := db.QueryRow(
		`SELECT COUNT(*) as exists FROM daily_stats 
        WHERE topic = $1 AND interval = $2`,
		s.Topic, s.intTime).Scan(&exists)
	if err != nil {
		return err
	}

	if exists == 0 {
		stmt, err := db.Prepare(`INSERT INTO daily_stats 
            (interval, topic, users, total) VALUES ($1, $2, hll_empty(), 0)`)
		if err != nil {
			return err
		}

		_, err = stmt.Exec(s.intTime, s.Topic)
		if err != nil {
			return err
		}
	}

	stmt, err := db.Prepare(`UPDATE daily_stats SET total = total + 1, 
        users = hll_add(users, hll_hash_text($1)) 
        WHERE topic = $2 AND interval = $3`)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(s.Fingerprint, s.Topic, s.intTime)
	if err != nil {
		return err
	}

	return nil
}

func handle_stat(m *MeatTruck) error {
	frames, err := m.Receiver.RecvMessage(0)
	if err != nil {
		return err
	}

	if len(frames) != 2 {
		return err
	}

	stat := new(StatMsg)
	err = stat.LoadJSON(frames[0], frames[1])
	if err != nil {
		return err
	}

	err = stat.Persist(m.Dbconn)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	truck := new(MeatTruck)
	if err := truck.LoadConf("./conf.json"); err != nil {
		log.Fatal(err)
	}

	db, err := sql.Open("postgres", truck.ConnInfo)
	if err != nil {
		log.Fatal(err)
	}

	truck.Dbconn = db
	truck.Receiver, _ = zmq.NewSocket(zmq.SUB)
	truck.Receiver.SetSubscribe("")
	defer truck.Receiver.Close()
	truck.Receiver.Connect("ipc:///tmp/forward_truck")

	truck.Reactor = zmq.NewReactor()
	truck.Reactor.AddSocket(truck.Receiver, zmq.POLLIN,
		func(e zmq.State) error { return handle_stat(truck) })

	err = truck.Reactor.Run(-1)
	log.Print(err)
}
